#pragma once

#include <iostream>
#include <string>

class Entity
{
public:
	Entity(int health, int xp, int attack, std::string name, char character);
	Entity(int health, int xp, int attack, std::string name);
	Entity(int health);
	Entity(int health, int xp);
	Entity(int health, int xp, int attack);
	Entity();
	virtual ~Entity();

	virtual int get_health() const;
	virtual void mod_health(int modAmount);
	virtual void set_health(int newHealth);

	virtual int get_attack() const;
	virtual void set_attack(int newAttack);
	virtual int calc_attack() = 0;

	virtual std::string get_name() const;
	virtual void set_name(std::string newName);

	virtual int get_xp() const;
	virtual void mod_xp(int modAmount);
	virtual void set_xp(int newAmount);

	virtual std::pair<int, int> get_location() const;
	virtual void set_location(std::pair<int, int> newLocation);

	virtual char get_character() const;
	virtual void set_character(char newChar);

protected:
	int health;
	int attack;
	int xp;
	std::string name;
	char character;
	std::pair<int, int> location;
private:
};

