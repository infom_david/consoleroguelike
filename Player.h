#pragma once
#include <utility>
#include "Entity.h"


class Player :
    public Entity
{
public: 

	Player();
	virtual ~Player();

	virtual void mod_xp(int modAmount) override;
	virtual void set_xp(int newAmount) override;

	virtual int calc_attack() override;

	virtual char get_character() const override;

	bool pickupItem(char item);

	int totalGold = 0;
	int maxHealth;
	int moveNum;


protected:
	int currentLevel;

private:

};

