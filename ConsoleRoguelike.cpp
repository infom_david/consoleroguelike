
#include <iostream>
#include <memory>
#include "GameController.h"
#include "Player.h"
#include "Monster.h"

int main()
{
	GameController newGame;

	newGame.runGame();

	return 0;
}
