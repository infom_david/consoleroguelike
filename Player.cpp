#include "Player.h"

Player::Player()
	: currentLevel{ 1 }, moveNum{ 0 }, totalGold{ 0 }, Entity() {
	character = 'P';
	health = 12;
	maxHealth = 10;
}

Player::~Player() { }

void Player::set_xp(int newAmount) {
	xp = newAmount;
}
void Player::mod_xp(int modAmount) {
	std::cout << "Player has gained " << modAmount << " xp. ";
	
	xp += modAmount;
	if (xp >= currentLevel * 50) {
		currentLevel++;
		std::cout << "\n ===== LEVEL UP =====" << std::endl;
		std::cout << " ===== You are now level " << currentLevel << " =====" << std::endl;
		maxHealth += ((currentLevel * 2) + 1);
		set_health(maxHealth);
		set_attack(get_attack() + 2);
		std::cout << " ===== Max HP Increased, HP Replenished, Base Attack Increased =====" << std::endl;
		
	}
}
char Player::get_character() const {
	return character;
}

int Player::calc_attack() {
	int baseAttack{ attack };
	int calculatedAttack{ 0 };
	int hitChance{ (rand() % 9) + 1 };

	if (hitChance != 1)
	{
		baseAttack += ( baseAttack + (hitChance / 3 ));
		return baseAttack;
	}
	else {
		return 0;
	}
}

bool Player::pickupItem(char item) {
	if (item == '^') {
		std::cout << "Gained 10 HP from potion. " << std::endl;
		mod_health(10);
	}
	else if (item == 'G') {
		int goldAmount{ (rand() % 100) + 10 };
		std::cout << "Gained " << goldAmount << " Gold. " << std::endl;
		totalGold += goldAmount;

	}

	return true;
}