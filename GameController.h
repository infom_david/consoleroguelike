#pragma once
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <conio.h>
#include "Player.h"
#include "Monster.h"

class GameController {
public:
	GameController();
	//~GameController();

	bool runGame();
	bool displayMenu();
	//bool setMode();
	int generateRandCoord();
	bool checkIfOccupied(int row, int col);
	bool generateMap();
	bool generateMap(int playerRow, int playerCol, int numEnemies);
	bool boundsCheck(int x, int y);
	bool refreshScreen();
	char playerInput();
	bool handleObstacle(int row, int col, bool isPlayer);
	void runCombat(int row, int col, bool playerInit);
	void getPlayerStats();
	void setupMonsters(int numEnemies);

	bool initScreenState();
	std::vector<std::vector<char>> getScreenState() { return screenState; }
	int startingRows{ 12 };
	int rowBounds;
	int startingCols{ 12 };
	int colBounds;


	std::unique_ptr<Player> currentPlayer;
	std::vector<std::unique_ptr<Monster>> monstersPresent;
	int playerRow;
	int playerCol;
	int floorNum{ 1 };
	int turnsTilMove{ 2 };
	bool playerStationary{ true };
	const char potionChar{ '^' };
	const char chestChar{ 'G' };
	

protected:

	std::vector<std::vector<char>> screenState;

private:
};