#include "Entity.h"
Entity::Entity(int health, int xp, int attack, std::string name, char character)
	: health{ health }, xp{ xp }, attack{ attack }, name{ name }, location{ 1, 1 }, character{ character } {}

Entity::Entity(int health, int xp, int attack, std::string name)
	: health{ health }, xp{ xp }, attack{ attack }, name{ name }, location{ 1, 1 }, character{ '?' } {}

Entity::Entity(int health, int xp, int attack)
	: Entity(health, xp, attack, "No Name") {}

Entity::Entity(int health, int xp)
	: Entity(health, xp, 1, "No Name") {}

Entity::Entity(int health)
	: Entity(health, 0, 1, "No Name") {}

Entity::Entity()
	: Entity(1, 0, 1, "No Name") {}

Entity::~Entity(){ }


int Entity::get_health() const {
	return health;
}
void Entity::mod_health(int modAmount) {
	health += modAmount;
}
void Entity::set_health(int newHealth) {
	health = newHealth;
}

int Entity::get_attack() const {
	return attack;
}
void Entity::set_attack(int newAttack) {
	attack = newAttack;
}

std::string Entity::get_name() const {
	return name;
}
void Entity::set_name(std::string newName) {
	name = newName;
}

int Entity::get_xp() const {
	return xp;
}

void Entity::mod_xp(int modAmount) {
	std::cout << "Cannot mod xp on an entity." << std::endl;
}
void Entity::set_xp(int newAmount) {
	std::cout << "Cannot set xp on an entity." << std::endl;
}

std::pair<int, int> Entity::get_location() const {
	return location;
}
void Entity::set_location(std::pair<int, int> newLocation) {
	location = newLocation;
}

void Entity::set_character(char newChar) {
	character = newChar;
}
char Entity::get_character() const {
	return character;
}
