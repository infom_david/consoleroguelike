#include "GameController.h"

GameController::GameController() {
    initScreenState();
    currentPlayer = std::make_unique<Player>();
}

bool GameController::runGame()
{
    bool running{ true };
    char choice{ 'r' };

    srand((unsigned)time(0));

    displayMenu();

    generateMap();
    
    while (running) {
         choice = playerInput();
        
        if (choice == 'q')
        {
            running = false;
        }
    }
    return true;
}

bool GameController::displayMenu() {
    char play{ ' ' };
    std::cout << "Welcome to a very simple, console-based roguelike." << std::endl << std::endl;
    std::cout << "Enemies will move after every other movement of yours, run into them to damage them." << std::endl;
    std::cout << "Once all enemies on a floor are defeated, you'll automatically descend to the next floor." << std::endl;
    std::cout << "(so make sure you grab any gold before you kill all of the enemies!)" << std::endl << std::endl;
    std::cout << "Key: " << std::endl;
    std::cout << "P = Player " << std::endl;
    std::cout << "M = Monster " << std::endl;
    std::cout << "G = Treasure " << std::endl;
    std::cout << "^ = Potion " << std::endl;
    std::cout << std::endl << "How low can you go?" << std::endl << std::endl;
    std::cout << "Press any key to play." << std::endl << std::endl;
    play = _getch();

    return true;
}

bool GameController::initScreenState() {
    screenState = std::vector<std::vector<char>>(startingRows, std::vector<char>(startingCols));
    rowBounds = startingRows - 1;
    colBounds = startingCols - 1;

    for (int i = 0; i < startingRows; ++i) {
        for (int j = 0; j < startingCols; ++j) {
            if (i == 0) {
                screenState[i][j] = '_';
            } else if (i == rowBounds) {
                screenState[i][j] = '_';
            } else if (j == 0) {
                screenState[i][j] = '|';
            } else if (j == colBounds) {
                screenState[i][j] = '|';
            } else {
                screenState[i][j] = ' ';
            }
        }
    }
    return true;
}
bool GameController::refreshScreen()
{
    bool enemiesDead { true };
    bool leave{ false };
    for (const auto& m : monstersPresent) {
        if (m->isAlive)
        {
            enemiesDead = false;
            leave = true;
            break;
        }
        if (leave) break;
    }
    if (enemiesDead) {
        std::cout << "\nAll Enemies Defeated!" << std::endl;
        floorNum++;
        std::cout << "Descending to floor " << floorNum <<  "..." << std::endl;

        enemiesDead = false;
        return true;
    }
    else {
        if (currentPlayer->moveNum % turnsTilMove == 0 && !playerStationary)
        {
            std::pair<int, int> pLoc{ currentPlayer->get_location() };

            for (const auto& m : monstersPresent) {
                if (m->isAlive) {
                    int randDirection{ (rand() % 2) + 1 };
                    bool monMoved{ false };
                    std::pair<int, int> mLoc{ m->get_location() };
                    if (randDirection == 1)
                    {
                        if (mLoc.first > pLoc.first) {
                            mLoc.first--;
                            monMoved = true;
                        }
                        else if (mLoc.first < pLoc.first) {
                            mLoc.first++;
                            monMoved = true;
                        }
                    }
                    else {
                        if (mLoc.second > pLoc.second) {
                            mLoc.second--;
                            monMoved = true;
                        }
                        else if (mLoc.second < pLoc.second) {
                            mLoc.second++;
                            monMoved = true;
                        }
                    }
                    // Account for randDir picking a direction that the monster doesn't need to move in
                    if (!monMoved) {
                        if (mLoc.first > pLoc.first) {
                            mLoc.first--;
                            monMoved = true;
                        }
                        else if (mLoc.first < pLoc.first) {
                            mLoc.first++;
                        }
                        if (mLoc.second > pLoc.second) {
                            mLoc.second--;
                        }
                        else if (mLoc.second < pLoc.second) {
                            mLoc.second++;
                            monMoved = true;
                        }
                    }


                    if (!checkIfOccupied(mLoc.first, mLoc.second))
                    {
                        screenState[m->get_location().first][m->get_location().second] = ' ';
                        m->set_location(mLoc);
                        screenState[mLoc.first][mLoc.second] = 'M';
                    }
                    else {
                        handleObstacle(mLoc.first, mLoc.second, false);
                    }

                }

            }
        }

        std::vector<std::vector<char>> screenState = getScreenState();
        std::cout << std::endl;
        for (int i = 0; i < startingRows; ++i) {
            for (int j = 0; j < startingCols; ++j) {
                std::cout << screenState[i][j];
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
        return false;
    }
}


bool GameController::checkIfOccupied(int row, int col) {
    if (screenState[row][col] != ' ')
        return true;
    else
        return false;
}


bool GameController::boundsCheck(int x, int y) {
    bool withinBounds = true;
    if (x == startingRows || y == startingCols) withinBounds = false;
    if (x == 0 || y == 0) withinBounds = false;

    return withinBounds;
}

int GameController::generateRandCoord() {
    int randy { (rand() % rowBounds) + 1 };
    if (randy >= rowBounds) randy -= 1;

    return randy;
}

void GameController::setupMonsters(int numEnemies) {
    std::vector<std::unique_ptr<Monster>> temp(numEnemies);
    int hp{ 1 };
    int xp{ 1 };
    int attack { 1 };

    for (auto& m : temp) {
        hp = (rand() % 5) + 1 * floorNum;
        xp = (rand() % 20) + 1 + floorNum;
        attack = (rand() % 3) * floorNum;
        m = std::make_unique<Monster>(hp, xp, attack);

    }

    monstersPresent.swap(temp);

    std::pair <int, int> enemyPosition;

    for (auto& m : monstersPresent) {
        enemyPosition.first = generateRandCoord();
        enemyPosition.second = generateRandCoord();
        while (checkIfOccupied(enemyPosition.first, enemyPosition.second)) {
            enemyPosition.first = generateRandCoord();
            enemyPosition.second = generateRandCoord();
        }
        std::pair<int, int> newLoc{ enemyPosition.first, enemyPosition.second };
        m->set_location(enemyPosition);

        screenState[m->get_location().first][m->get_location().second] = m->get_character();
    }
}

bool GameController::generateMap(int setPlayerRow, int setPlayerCol, int numEnemies) {

    std::cout << "Loading Map with " << numEnemies << " enemies..." << std::endl << std::endl;

    std::cout << "Floor Number: " << floorNum << std::endl;

    std::pair<int, int> playerStartingLoc{ setPlayerRow , setPlayerCol };
    currentPlayer->set_location(playerStartingLoc);
    screenState[playerStartingLoc.first][playerStartingLoc.second] = 'P';

    setupMonsters(numEnemies);

    std::pair <int, int> potionPosition;
    do {
        potionPosition.first = generateRandCoord();
        potionPosition.second = generateRandCoord();
    } while (checkIfOccupied(potionPosition.first, potionPosition.second));

    int numberOfChests{ (rand() % 3) };
    for (int i = 0; i < numberOfChests; ++i) {
        std::pair <int, int> chestPosition;
        do {
            chestPosition.first = generateRandCoord();
            chestPosition.second = generateRandCoord();
        } while (checkIfOccupied(chestPosition.first, chestPosition.second));
        screenState[chestPosition.first][chestPosition.second] = chestChar;
    }




    refreshScreen();

    return true;
}

bool GameController::generateMap() {
    int randRow{ generateRandCoord() };
    int randCol{ generateRandCoord() };
    int numEnemies{ (rand() % 4) + 1 };

    return generateMap(randRow, randCol, numEnemies);

}

char GameController::playerInput() {
    char input;
    bool moved{ false };
    bool quitting{ false };
    bool selected { false };
    playerStationary = true;
    std::string outputText{""};
    std::pair<int, int> playerLocation = currentPlayer->get_location();
    int playerRow = playerLocation.first;
    int playerCol = playerLocation.second;

    while (!selected) {
        std::cout << "Move: wasd || View Stats: e || Refresh Map: r || Quit: q \n" << std::endl;
        std::cout << "Input: ";
        input = _getch();
        switch (input) {
            case 'w':
                if (!checkIfOccupied(playerRow - 1, playerCol)) {
                    screenState[playerRow][playerCol] = ' ';
                    playerRow -= 1;
                    moved = true;
                    outputText = "\nMoved up.\n";
                } else {
                    handleObstacle((playerRow - 1), playerCol, true);
                }
                selected = true;
                break;
            case 'a':
                if (!checkIfOccupied(playerRow, playerCol - 1)) {
                    screenState[playerRow][playerCol] = ' ';
                    playerCol -= 1;
                    moved = true;
                    outputText = "\nMoved left.\n";
                } else {
                    handleObstacle(playerRow, (playerCol - 1), true);
                }
                selected = true;
                break;
            case 's':
                if (!checkIfOccupied(playerRow + 1, playerCol)) {
                    screenState[playerRow][playerCol] = ' ';
                    playerRow += 1;
                    moved = true;
                    outputText = "\nMoved down.\n";
                } else {
                    handleObstacle((playerRow + 1), playerCol, true);
                }
                selected = true;
                break;
            case 'd':
                if (!checkIfOccupied(playerRow, playerCol + 1)) {
                    screenState[playerRow][playerCol] = ' ';
                    playerCol += 1;
                    moved = true;
                    outputText = "\nMoved right.\n";
                } else {
                    handleObstacle(playerRow,(playerCol + 1), true);
                }
                selected = true;
                break;
            case 'r':
                selected = true;
                // No need to do anything since we refresh below
                outputText = "\nScreen refreshed.\n";
                break;
            case 'e':
                selected = true;
                getPlayerStats();
                outputText = "\nDisplayed stats.\n";
                break;
            case 'q':
                selected = true;
                quitting = true;
                outputText = "\nQuitting game...\n";
                break;
            default:
                std::cout << "Invalid key, try again" << std::endl;
                break;
        }
    }

    if (moved) {
        playerStationary = false;
        currentPlayer->moveNum++;
        std::pair<int, int> newLocation{ playerRow, playerCol };
        std::cout << std::endl << "Current Location: " << newLocation.first << " " << newLocation.second << std::endl;
        currentPlayer->set_location(newLocation);
        screenState[newLocation.first][newLocation.second] = currentPlayer->get_character();
    }
    if (currentPlayer->get_health() <= 0) {
        outputText = "\n===== GAME OVER! =====\n";
        getPlayerStats();
        quitting = true;
        input = 'q';
    }


    if (!quitting) {
        bool levelComplete = refreshScreen();
        if (levelComplete == true)
        {
            initScreenState();
            generateMap();
            outputText = "New Floor Loaded\n";
        }
    }


    std::cout << outputText;
    return input;
}

bool GameController::handleObstacle(int row, int col, bool isPlayer) {
    char obstacle = screenState[row][col];

    if (obstacle == 'M')
    {
        if (isPlayer)
            runCombat(row, col, true);
    }
    else if (obstacle == 'P') {
        if (!isPlayer)
        {
            runCombat(row, col, false);
        }
    }
    else if (obstacle == potionChar) {
        if (isPlayer) {
            currentPlayer->pickupItem(potionChar);
            screenState[row][col] = ' ';
        }
    }
    else if (obstacle == chestChar) {
        if (isPlayer) {
            currentPlayer->pickupItem(chestChar);
            screenState[row][col] = ' ';
        }
    }
    else {
        std::cout << "\nObstacle, try again" << std::endl;
    }
    return true;
}

void GameController::runCombat(int row, int col, bool playerInit) {
  
    bool combatComplete{ false };
    for (auto const& m : monstersPresent) {
        if (playerInit)
        {
            std::pair<int, int> loc{ m->get_location() };
            if (loc.first == row && loc.second == col) {
                int playerAttack{ currentPlayer->calc_attack() };
                m->mod_health(-(playerAttack));
                if(playerAttack <= 0 )
                    std::cout << "Player misses!" << std::endl;
                else
                    std::cout << "Player hits monster for " << playerAttack << " damage." << std::endl;

                int monAttack{ m->get_attack() };
                currentPlayer->mod_health(-(m->get_attack()));
                if (monAttack <= 0)
                    std::cout << "Monster misses!" << std::endl;
                else
                    std::cout << "Monster hits player for " << monAttack << " damage." << std::endl;

                if (m->get_health() <= 0)
                {
                    std::cout << "Monster has died." << std::endl;
                    m->isAlive = false;
                    screenState[m->get_location().first][m->get_location().second] = ' ';
                    currentPlayer->mod_xp(m->get_xp());
                }
                else {
                    std::cout << "Monster has " << m->get_health() << " hp left." << std::endl;
                }
                if (currentPlayer->get_health() <= 0)
                {
                    std::cout << "Player has died." << std::endl;
                }
                else {
                    std::cout << "Player has " << currentPlayer->get_health() << " hp left." << std::endl;
                }
            }
            combatComplete = true;
        }
        else if(combatComplete == false) {
            std::pair<int, int> loc{ m->get_location() };
            runCombat(loc.first, loc.second, true);
            combatComplete = true;
        }

    }
}


void GameController::getPlayerStats() {
    std::cout << "Player Stats: HP:" << currentPlayer->get_health() << " | " << " XP:" << currentPlayer->get_xp()
        << " | " << " Gold:" << currentPlayer->totalGold 
        << " | " << " Base Attack:" << currentPlayer->get_attack() << std::endl;
}