#pragma once
#include <string>
#include "Entity.h"
class Monster :
    public Entity
{
public:
    Monster();
    Monster(int health);
    Monster(int health, int xp);
    Monster(int health, int xp, int attack);
    Monster(int health, int xp, int attack, std::string name);
    Monster(int health, int xp, int attack, std::string name, std::string monsterType);
    Monster(int health, int xp, int attack, std::string name, char character);
    Monster(int health, int xp, int attack, std::string name, std::string monsterType, char character);

    virtual ~Monster();

    char defaultChar = 'M';
    std::string defaultType = "Generic Monster";
    std::string monsterType;
    bool isAlive;

    virtual char get_character() const override;
    virtual int calc_attack() override;

protected:
    
private:
};

