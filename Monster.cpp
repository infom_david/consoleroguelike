#include "Monster.h"

Monster::Monster()
	: monsterType{ defaultType }, isAlive{ true }, Entity(1, 10, 1, "No Name", 'M') {}

Monster::Monster(int health)
	: monsterType{ defaultType }, isAlive{ true }, Entity(health, 1, 1, "No Name", 'M') {}

Monster::Monster(int health, int xp)
	: monsterType{ defaultType }, isAlive{ true }, Entity(health, xp, 1, "No Name", 'M') {}

Monster::Monster(int health, int xp, int attack)
	: monsterType{ defaultType }, isAlive{ true }, Entity(health, xp, attack, "No Name", 'M') {}

Monster::Monster(int health, int xp, int attack, std::string name)
	: monsterType{ defaultType }, isAlive{ true }, Entity(health, xp, attack, name, 'M') {}

Monster::Monster(int health, int xp, int attack, std::string name, char character)
	: monsterType{ defaultType }, isAlive{ true }, Entity(health, xp, attack, name, character) {}

Monster::Monster(int health, int xp, int attack, std::string name, std::string monsterType)
	: monsterType{ monsterType }, isAlive{ true }, Entity(health, xp, attack, name, 'M') {}

Monster::Monster(int health, int xp, int attack, std::string name, std::string monsterType, char character)
	: monsterType{ monsterType }, isAlive{ true }, Entity(health, xp, attack, name, character) {}


Monster::~Monster() { }

char Monster::get_character() const {
	return character;
}

int Monster::calc_attack() {
	int baseAttack{ attack };
	int calculatedAttack{ 0 };
	int hitChance{ (rand() % 9) + 1 };

	if (hitChance != 1)
	{
		baseAttack += (baseAttack + (hitChance / 3));
		return baseAttack;
	}
	else {
		return 0;
	}
}